#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QSharedMemory>
#include <QUdpSocket>

#include "client/SWGSettingsApi.h"
#include "client/SWGStatesApi.h"
#include "client/SWGGroupsApi.h"

#include <memory>

namespace Ui {
  class MainWindow;
}

using namespace Swagger;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  void Init(const QString host, const QString port);

public slots:
  void exitHandler();

protected:
  void changeEvent(QEvent *e);

private slots:
  void on_sbFramerate_valueChanged(int arg1);
  void onScreenshotTimerFired();

  void onSettingsReceived(SWGSettings* out);
  void onGetSettingsError(SWGSettings* summary, QNetworkReply::NetworkError error_type, QString& error_str);
  void onSettingsUpdated();
  void onSetSettingsError(QNetworkReply::NetworkError error_type, QString& error_str);

  void onStatesReceived(SWGStates* out);
  void onGetStatesError(SWGStates* summary, QNetworkReply::NetworkError error_type, QString& error_str);
  void onStatesUpdated();
  void onSetStatesError(QNetworkReply::NetworkError error_type, QString& error_str);

  void onGroupsReceived(QList<SWGGroup*>* out);
  void onGetGroupsError(QList<SWGGroup*>* summary, QNetworkReply::NetworkError error_type, QString& error_str);

  void on_tbCapturing_clicked();
  void on_pbSyncing_clicked();

  void on_sbMinBri_valueChanged(int arg1);
  void on_sbMaxBri_valueChanged(int arg1);
  void on_sbColorBias_valueChanged(int arg1);
  void on_sbLumaBoost_valueChanged(int arg1);
  void on_sbChromaBoost_valueChanged(int arg1);
  void on_sbCapMult_valueChanged(int arg1);

  void on_actionOpen_triggered();

private:
  Ui::MainWindow *ui;
  QString m_hue_host;
  QString m_hue_port;

  void handlePixmap();
  void CalculateAverageColor(QImage& img, QColor& dominant);
  void CalculateDominantColor(QImage& img, QColor& dominant);

  QPixmap originalPixmap;

  int m_fps = 0;
  QTimer m_screenshotTimer;

  std::unique_ptr<SWGSettingsApi> m_api_settings;
  std::unique_ptr<SWGStatesApi> m_api_states;
  std::unique_ptr<SWGGroupsApi> m_api_groups;

  QSharedMemory m_hueSM;
  QUdpSocket m_client_socket;
  QString m_sel_group;
};

#endif // MAINWINDOW_H
