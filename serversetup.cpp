#include "serversetup.h"
#include "ui_serversetup.h"

ServerSetup::ServerSetup(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ServerSetup)
{
  ui->setupUi(this);
}

ServerSetup::~ServerSetup()
{
  delete ui;
}
