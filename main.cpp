#include "MainWindow.h"
#include <QApplication>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MainWindow w;
  QObject::connect(&a, SIGNAL(aboutToQuit()), &w, SLOT(exitHandler()));

  QCommandLineParser parser;
  parser.setApplicationDescription("huetainment GUI");
  parser.addHelpOption();
  parser.addVersionOption();

  QCommandLineOption hostOption(QStringList() << "H" << "hostname",
          QCoreApplication::translate("main", "huetainment host."),
                                "hostname",
                                "localhost");
  parser.addOption(hostOption);

  QCommandLineOption portOption(QStringList() << "p" << "port",
          QCoreApplication::translate("main", "huetainment port."),
                                "port",
                                "3001");
  parser.addOption(portOption);

  parser.process(a);

  w.Init(parser.value(hostOption), parser.value(portOption));
  w.show();

  return a.exec();
}
