#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QtWidgets>

#include "utility.h"

#define SHARED_KEY "huetainmentSharedImage"
#define UDP_PORT 3002

#define UNIT_WIDTH 16
#define UNIT_HEIGHT 9

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , m_hueSM(SHARED_KEY, this)
  , m_client_socket(this)
{
  ui->setupUi(this);
}

MainWindow::~MainWindow()
{
  m_hueSM.detach();
  delete ui;
}

void MainWindow::Init(const QString host, const QString port)
{
  if (!m_hueSM.attach())
    qCritical() << "Cannot attach SM";

  qDebug() << host << port;
  m_hue_host = host;
  m_hue_port = port;

  QString api_host = "http://" + m_hue_host + ":" + m_hue_port;
  QString api_path = "/api/v1";

  m_api_settings.reset(new SWGSettingsApi(api_host, api_path));
  connect(m_api_settings.get(), SIGNAL(getSettingsSignal(SWGSettings*)), this, SLOT(onSettingsReceived(SWGSettings*)));
  connect(m_api_settings.get(), SIGNAL(getSettingsSignalE(SWGSettings*, QNetworkReply::NetworkError, QString&)),
          this, SLOT(onGetSettingsError(SWGSettings*, QNetworkReply::NetworkError, QString&)));
  connect(m_api_settings.get(), SIGNAL(setSettingsSignal()), this, SLOT(onSettingsUpdated()));
  connect(m_api_settings.get(), SIGNAL(setSettingsSignalE(QNetworkReply::NetworkError, QString&)),
          this, SLOT(onSetSettingsError(QNetworkReply::NetworkError, QString&)));
  m_api_settings->getSettings();

  m_api_states.reset(new SWGStatesApi(api_host, api_path));
  connect(m_api_states.get(), SIGNAL(getStatesSignal(SWGStates*)), this, SLOT(onStatesReceived(SWGStates*)));
  connect(m_api_states.get(), SIGNAL(getStatesSignalE(SWGStates*, QNetworkReply::NetworkError, QString&)),
          this, SLOT(onGetStatesError(SWGStates*, QNetworkReply::NetworkError, QString&)));
  connect(m_api_states.get(), SIGNAL(setStatesSignal()), this, SLOT(onStatesUpdated()));
  connect(m_api_states.get(), SIGNAL(setStatesSignalE(QNetworkReply::NetworkError, QString&)),
          this, SLOT(onSetStatesError(QNetworkReply::NetworkError, QString&)));
  m_api_states->getStates();

  m_api_groups.reset(new SWGGroupsApi(api_host, api_path));
  connect(m_api_groups.get(), SIGNAL(getGroupsSignal(QList<SWGGroup*>*)), this, SLOT(onGroupsReceived(QList<SWGGroup*>*)));
  connect(m_api_groups.get(), SIGNAL(getGroupsSignalE(QList<SWGGroup*>*, QNetworkReply::NetworkError, QString&)),
          this, SLOT(onGetGroupsError(QList<SWGGroup*>*, QNetworkReply::NetworkError, QString&)));
  m_api_groups->getGroups();

  ui->sbFramerate->setValue(m_fps);

  connect(&m_screenshotTimer, SIGNAL(timeout()), this, SLOT(onScreenshotTimerFired()));
}

void MainWindow::exitHandler()
{
  if (ui->pbSyncing->isChecked())
  {
    QTimer timer;
    timer.setSingleShot(true);
    QEventLoop loop;
    connect(m_api_states.get(), SIGNAL(setStatesSignal()), &loop, SLOT(quit()) );
    connect(m_api_states.get(), SIGNAL(setStatesSignalE(QNetworkReply::NetworkError , QString& )), &loop, SLOT(quit()) );
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    timer.start(5000);

    SWGStates states;
    states.setSyncing(false);
    m_api_states->setStates(states);

    loop.exec();
  }
}

void MainWindow::changeEvent(QEvent *e)
{
  QMainWindow::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

void MainWindow::on_sbFramerate_valueChanged(int arg1)
{
  m_fps = arg1;
  if (m_fps)
  {
    if (m_screenshotTimer.isActive())
      m_screenshotTimer.start(1.0 / m_fps * 1000);
  }
  else
    m_screenshotTimer.stop();
}

void MainWindow::CalculateAverageColor(QImage& screenSyncScreen, QColor& dominant)
{
  int minX = 0;
  int minY = 0;
  int maxX = screenSyncScreen.width() -1;
  int maxY = screenSyncScreen.height() -1;

  double tavgR = 0.0, tavgG = 0.0, tavgB = 0.0;
  double tdomR = 0.0, tdomG = 0.0, tdomB = 0.0;
  int cols = maxX - minX;
  int rows = 0;
  int dom_samples = 0;

  for (int y = minY; y < maxY; ++y)
  {
    double rR = 0.0, rG = 0.0, rB = 0.0;

    for (int x = minX; x < maxX; ++x)
    {
      double pR = (double)qRed(screenSyncScreen.pixel(x, y)) / 255.0;
      double pG = (double)qGreen(screenSyncScreen.pixel(x, y)) / 255.0;
      double pB = (double)qBlue(screenSyncScreen.pixel(x, y)) / 255.0;

      rR += pR; rG += pG; rB += pB;

      double min = std::min(pR, std::min(pG, pB));
      double max = std::max(pR, std::max(pG, pB));
      double pS = max == 0 ? 0 : (max - min) / max;
      if (pS > 0.01)
      {
        tdomR += pR;
        tdomG += pG;
        tdomB += pB;
        ++dom_samples;
      }
    }
    rR /= cols; rG /= cols; rB /= cols;

    // ignore black rows
    double rY = (0.2126 *rR + 0.7152 *rG + 0.0722 *rB);
    if (rY > 0.01)
    {
      tavgR += rR;
      tavgG += rG;
      tavgB += rB;
      ++rows;
    }
  }

  tavgR /= rows; tavgG /= rows; tavgB /= rows;
  double use_brightness = (0.2126 *tavgR + 0.7152 *tavgG + 0.0722 *tavgB);
  if (dom_samples > rows * cols / 4) // If more than 25% usable pixels -> chroma
  {
//      qInfo() << "DOM" << dom_samples;
    tavgR = tdomR / dom_samples; tavgG = tdomG / dom_samples; tavgB = tdomB / dom_samples;
  }
  else
  {
//      qInfo() << "AVG" << rows;
  }

  double cx, cy, cb;
  Color::rgb2xyB(tavgR, tavgG, tavgB, cx, cy, cb);

  double minBrightness = (double)ui->sbMinBri->value() / 100.0;
  double maxBrightness = (double)ui->sbMaxBri->value() / 100.0;
  double brightnessBoost = (double)ui->sbLumaBoost->value() / 100.0;

  double min = std::min(tavgR, std::min(tavgG, tavgB));
  double max = std::max(tavgR, std::max(tavgG, tavgB));
  double S = max == 0 ? 0 : (max - min) / max;
  if (S < 0.01) // ignore brigtness adjustments in low saturation
  {
    cb = std::min(cb, use_brightness);
  }
  else
  {
    double brightness = std::min(1.0, use_brightness * brightnessBoost);
    cb = brightness * (maxBrightness - minBrightness) + minBrightness;
  }

  double r,g, b;
  Color::xyB2rgb(cx, cy, cb, r, g, b);
//  qDebug() << "tR:" << round(tR * 255) << "tG:" << round(tG * 255) << "tB:" << round(tB * 255) << "x:" << cx << "y:" << cy << "B:" << cb << "r:" << r << "g:" << g << "b:" << b;
  dominant.setRgbF(r, g, b);
}

void MainWindow::CalculateDominantColor(QImage& screenSyncScreen, QColor& dominant)
{
  double tR = 0.0, tG = 0.0, tB = 0.0;
  int samples = 0;

  for (int y = 0; y < screenSyncScreen.height(); ++y)
  {
    for (int x = 0; x < screenSyncScreen.width(); ++x)
    {
      double pR = (double)qRed(screenSyncScreen.pixel(x, y)) / 255.0;
      double pG = (double)qGreen(screenSyncScreen.pixel(x, y)) / 255.0;
      double pB = (double)qBlue(screenSyncScreen.pixel(x, y)) / 255.0;

      double min = std::min(pR, std::min(pG, pB));
      double max = std::max(pR, std::max(pG, pB));
      double pS = max == 0 ? 0 : (max - min) / max;
      if (pS > (1.0 - (double)ui->sbChromaBoost->value() / 100))
      {
        tR += pR;
        tG += pG;
        tB += pB;
        ++samples;
      }
    }
  }

  tR /= samples; tG /= samples; tB /= samples;
  double cx, cy, cb;
  Color::rgb2xyB(tR, tG, tB, cx, cy, cb);

  double minBrightness = (double)ui->sbMinBri->value() / 100.0;
  double maxBrightness = (double)ui->sbMaxBri->value() / 100.0;
  double brightness = std::min(1.0, cb * (double)ui->sbLumaBoost->value() / 100.0);
  cb = brightness * (maxBrightness - minBrightness) + minBrightness;

  double r,g, b;
  Color::xyB2rgb(cx, cy, cb, r, g, b);
//  qDebug() << "tR:" << round(tR * 255) << "tG:" << round(tG * 255) << "tB:" << round(tB * 255) << "x:" << cx << "y:" << cy << "B:" << cb << "r:" << r << "g:" << g << "b:" << b;
  dominant.setRgbF(r, g, b);
}

void MainWindow::onScreenshotTimerFired()
{
  static QElapsedTimer timer;
  QScreen *screen = QGuiApplication::primaryScreen();
  if (const QWindow *window = windowHandle())
      screen = window->screen();
  if (!screen)
      return;

  originalPixmap = screen->grabWindow(0).scaled(UNIT_WIDTH * ui->sbCapMult->value(),
                                            UNIT_HEIGHT * ui->sbCapMult->value(),
                                            Qt::IgnoreAspectRatio,
                                            Qt::FastTransformation);
  handlePixmap();
}

void MainWindow::handlePixmap()
{
  if (!isMinimized() && isVisible())
    ui->lScreenshot->setPixmap(originalPixmap.scaled(ui->lScreenshot->size(),
                                                   Qt::KeepAspectRatio,
                                                   Qt::FastTransformation));


//  qDebug() << "elapsed: " << timer.restart();

  QImage img = originalPixmap.toImage();

  QColor c;
  CalculateAverageColor(img, c);
  QPixmap avg(ui->lAvgColor->size());
  avg.fill(c);
  ui->lAvgColor->setPixmap(avg);

  CalculateDominantColor(img, c);
  QPixmap dom(ui->lDomColor->size());
  dom.fill(c);
  ui->lDomColor->setPixmap(dom);

  if (!ui->pbSyncing->isChecked())
    return;

#if 0
  if (!m_hueSM.isAttached() && !m_hueSM.attach())
    return;

  if (!m_hueSM.lock())
  {
    qCritical() << "Cannot lock SM";
    return;
  }

  QBuffer buf;
  buf.open(QBuffer::ReadWrite);
  QDataStream out(&buf);
  out << img;

  char *to = (char*)m_hueSM.data();
  const char *from = buf.data().data();
  memcpy(to, from, qMin(m_hueSM.size(), (int)buf.size()));
  buf.close();

  m_hueSM.unlock();
#else
  if (m_client_socket.isOpen())
  {
    QBuffer buf;
    buf.open(QBuffer::ReadWrite);
    QDataStream out(&buf);
    out << img;

    m_client_socket.write(buf.data().data(), buf.size());
//    qDebug() << "UDP:" << buf.size();
  }
#endif
}

void MainWindow::onSettingsReceived(SWGSettings* settings)
{
  ui->sbCapMult->setValue(settings->getCaptureMultiplier());
  ui->sbColorBias->setValue(settings->getColorBias());
  ui->sbMinBri->setValue(settings->getMinLuminance());
  ui->sbMaxBri->setValue(settings->getMaxLuminance());
  ui->sbLumaBoost->setValue(settings->getLumaBoost());
  ui->sbChromaBoost->setValue(settings->getChromaBoost());
  m_sel_group = *settings->getSyncingGroup();
}

void MainWindow::onGetSettingsError(SWGSettings* summary, QNetworkReply::NetworkError error_type, QString& error_str)
{
  qCritical() << "Could no read settings:" << error_str << " (" << (int)error_type << ")";
}

void MainWindow::onSettingsUpdated()
{
  qInfo() << "Settings updated";
  m_api_settings->getSettings();
}

void MainWindow::onSetSettingsError(QNetworkReply::NetworkError error_type, QString &error_str)
{
  qCritical() << "Could no set settings:" << error_str << " (" << (int)error_type << ")";
}

void MainWindow::onStatesReceived(SWGStates *states)
{ 
  if (states->isSyncing())
  {
    m_client_socket.connectToHost(m_hue_host, UDP_PORT, QIODevice::WriteOnly);
    if (!m_client_socket.waitForConnected(5000))
    {
      qCritical() << "Cannot connect:" << m_client_socket.errorString();
      SWGStates states;
      states.setSyncing(ui->pbSyncing->isChecked());
      m_api_states->setStates(states);
    }
    else
    {
      qInfo() << "Connected";
      ui->pbSyncing->setText("Stop Syncing");
    }
  }
  else
  {
    m_client_socket.close();
    ui->pbSyncing->setText("Start Syncing");
  }
}

void MainWindow::onGetStatesError(SWGStates *summary, QNetworkReply::NetworkError error_type, QString &error_str)
{
  qCritical() << "Could no read states:" << error_str << " (" << (int)error_type << ")";
}

void MainWindow::onStatesUpdated()
{
  qInfo() << "States updated";
  m_api_states->getStates();
}

void MainWindow::onSetStatesError(QNetworkReply::NetworkError error_type, QString &error_str)
{
  qCritical() << "Could no set states:" << error_str << " (" << (int)error_type << ")";
}

void MainWindow::onGroupsReceived(QList<SWGGroup*>* out)
{
  ui->cbGroups->clear();
  for (auto g : *out)
  {
    ui->cbGroups->addItem(*(g->getName()));
    if (*(g->getName()) == m_sel_group)
      ui->cbGroups->setCurrentIndex(ui->cbGroups->count()-1);
  }
}

void MainWindow::onGetGroupsError(QList<SWGGroup*>* summary, QNetworkReply::NetworkError error_type, QString& error_str)
{
  qCritical() << "Could no get groups:" << error_str << " (" << (int)error_type << ")";
}

void MainWindow::on_tbCapturing_clicked()
{
  if (m_screenshotTimer.isActive())
  {
    m_screenshotTimer.stop();
    ui->tbCapturing->setIcon(QIcon(":/icons/media-playback-start.png"));
  }
  else
  {
    if (m_fps)
    {
      m_screenshotTimer.start(1.0 / m_fps * 1000);
      ui->tbCapturing->setIcon(QIcon(":/icons/media-playback-pause.png"));
    }
  }
}

void MainWindow::on_pbSyncing_clicked()
{
  SWGStates states;
  if (ui->pbSyncing->isChecked())
  {
    states.setCaptureDevice(new QString("udp:"));
    states.setSyncingGroup(new QString(ui->cbGroups->currentText()));
  }
  states.setSyncing(ui->pbSyncing->isChecked());
  m_api_states->setStates(states);
}

void MainWindow::on_sbMinBri_valueChanged(int val)
{
  SWGSettings settings;
  settings.setMinLuminance(val);
  m_api_settings->setSettings(settings);
}

void MainWindow::on_sbMaxBri_valueChanged(int val)
{
  SWGSettings settings;
  settings.setMaxLuminance(val);
  m_api_settings->setSettings(settings);
}

void MainWindow::on_sbColorBias_valueChanged(int val)
{
  SWGSettings settings;
  settings.setColorBias(val);
  m_api_settings->setSettings(settings);
}

void MainWindow::on_sbLumaBoost_valueChanged(int val)
{
  SWGSettings settings;
  settings.setLumaBoost(val);
  m_api_settings->setSettings(settings);
}

void MainWindow::on_sbChromaBoost_valueChanged(int val)
{
  SWGSettings settings;
  settings.setChromaBoost(val);
  m_api_settings->setSettings(settings);

  handlePixmap();
}

void MainWindow::on_sbCapMult_valueChanged(int val)
{
  SWGSettings settings;
  settings.setCaptureMultiplier(val);
  m_api_settings->setSettings(settings);
}

void MainWindow::on_actionOpen_triggered()
{
  QString fileName = QFileDialog::getOpenFileName(this,
      tr("Open Image"), "",
      tr("All Files (*)"));

  if (fileName.isEmpty())
      return;
  else
  {
    originalPixmap.load(fileName);
    handlePixmap();
  }
}

