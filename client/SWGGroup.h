/**
 * huetainment API
 * huetainment API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: cbro@semperpax.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

/*
 * SWGGroup.h
 *
 * 
 */

#ifndef SWGGroup_H_
#define SWGGroup_H_

#include <QJsonObject>


#include <QString>

#include "SWGObject.h"

namespace Swagger {

class SWGGroup: public SWGObject {
public:
    SWGGroup();
    SWGGroup(QString json);
    ~SWGGroup();
    void init();
    void cleanup();

    QString asJson () override;
    QJsonObject asJsonObject() override;
    void fromJsonObject(QJsonObject json) override;
    SWGGroup* fromJson(QString jsonString) override;

    bool isIsStreaming();
    void setIsStreaming(bool is_streaming);

    QString* getName();
    void setName(QString* name);


    virtual bool isSet() override;

private:
    bool is_streaming;
    bool m_is_streaming_isSet;

    QString* name;
    bool m_name_isSet;

};

}

#endif /* SWGGroup_H_ */
