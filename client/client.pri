QT += network

HEADERS += \
# Models
    $${PWD}/SWGGroup.h \
    $${PWD}/SWGSettings.h \
    $${PWD}/SWGStates.h \
# APIs
    $${PWD}/SWGGroupsApi.h \
    $${PWD}/SWGSettingsApi.h \
    $${PWD}/SWGStatesApi.h \
# Others
    $${PWD}/SWGHelpers.h \
    $${PWD}/SWGHttpRequest.h \
    $${PWD}/SWGModelFactory.h \
    $${PWD}/SWGObject.h \
    $${PWD}/SWGQObjectWrapper.h

SOURCES += \
# Models
    $${PWD}/SWGGroup.cpp \
    $${PWD}/SWGSettings.cpp \
    $${PWD}/SWGStates.cpp \
# APIs
    $${PWD}/SWGGroupsApi.cpp \
    $${PWD}/SWGSettingsApi.cpp \
    $${PWD}/SWGStatesApi.cpp \
# Others
    $${PWD}/SWGHelpers.cpp \
    $${PWD}/SWGHttpRequest.cpp

