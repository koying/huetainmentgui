#ifndef SERVERSETUP_H
#define SERVERSETUP_H

#include <QDialog>

namespace Ui {
class ServerSetup;
}

class ServerSetup : public QDialog
{
  Q_OBJECT

public:
  explicit ServerSetup(QWidget *parent = nullptr);
  ~ServerSetup();

private:
  Ui::ServerSetup *ui;
};

#endif // SERVERSETUP_H
